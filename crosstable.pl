#!/usr/bin/perl
# $Id: crosstable.pl 265 2006-02-07 18:36:59Z holger $
#
# Copyright (C) 2005-2006 Holger Ruckdeschel <holger@hoicher.de>
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
# MA 02110-1301, USA.
#

use warnings;
use strict;

use Getopt::Long;

use Crosstable;

###############################################################################

my $print_sb = 0;
GetOptions("print-sb" => \$print_sb) or die;

my @pgnfiles = splice(@ARGV, 0);

Crosstable::print({ pgnfiles => \@pgnfiles,
		print_sb => $print_sb
	});

